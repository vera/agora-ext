try {
    // console.log("fail?")
    // import $ from jquery
    const agoraBase = "https://anagora.org/node/"
    // console.log("agora", agoraBase)




    const replaceStrings = (str) => {
        const wikireg = /\[\[(.*?)\]\]/g
        const out = str.matchAll(wikireg)

        for (const rep of out) {
            const slug = rep[1]
            const wiki = rep[0]
            const f = slug.toLowerCase().replace(" ", "-")
            str = str.replace(wiki, `<a href='${agoraBase}${f}'>${wiki}</a>`)
        }
        return str
    }

    // console.log(replaceStrings(text))


    // const parsePage = () => {
    //     let i = document.body.innerHTML
    //     i = "what the [[fuck]] is this"
    //     const parsed = replaceStrings(i)
    //     // console.log(parsed)
    //     // document.body.innerHTML = parsed
    //     // console.log(i)
    //     // setTimeout(()=>{ $('body').html(parsed)}, 3000)
    //     // $('body').html(parsed);

    // }

    setTimeout(function () {
        console.log("load script")
        const foo = $(":not(:has(*))").filter(function () {
            const txt = $(this).text()
            const wikireg = /\[\[(.*?)\]\]/g
            const match = wikireg.test(txt)
            if (!match) return
            // if (txt.length > 500) return
            return true;
        });
        for (const m of foo) {
            let t = $(m).text()
            const txt = replaceStrings(t)
            $(m).html(txt)
        }


    }, 1000)
} catch (e) {
    console.error(e)
}